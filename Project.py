#Вывод на экран кинозала
def printMatrix(matrix):
    print('Место №  ', end='')
    for i in range(len(matrix[0])):
        print(i + 1, ' ', end='')
    print()
    for i in range(len(matrix)):
        print(i + 1, 'ряд =', ' ', end='')
        for j in range(len(matrix[i])):
            print(matrix[i][j], ' ', end='')
        print()
def statistic(matrix): #Выводит количество ЗАНЯТЫХ мест
    k = 0
    s = 0
    for i in range(len(matrix)):
        for j in range(len(matrix[0])):
            k += 1
            if matrix[i][j] == 1:
                s += 1
    return s
#Покупка билета
def Poisk_mesta(file):
    with open(file) as f:
        line = f.readline().split()
        matrix = []
        m = int(line[0])
        n = int(line[1])
        for line in f:
            matrix.append([int(x) for x in line.split()])
    printMatrix(matrix)
    k = n * m
    print('Занято мест:', statistic(matrix), 'Всего мест:', k)
    besk = 1 #Вывод статистики
    if statistic(matrix) != k:
        while besk > 0:
            ####################################### Ввод позиции
            besk1 = 1
            while besk1 > 0:
                x = int(input('Введите ряд: '))
                y = int(input('Введите место: '))
                if 1 <= x <= 6 and 1 <= y <= 9:
                    break
                else:
                    print('Извините, но такого места не существует, выберите другое')

            ####################                Покупка билета               ###################
            if matrix[x - 1][y - 1] == 0:
                print('Это место свободно')
                matrix[x - 1][y - 1] = 1
                print('Поздравляем вы приобрели билет, ваше место', 'Ряд:', x, 'Место:', y)
                print('Занято мест:', statistic(matrix), 'Всего мест:', k)
                Zapolnenie(file, matrix)
                break
            else:
                print('Это место занято, выберите другое')
    else:
        print('Извините все места на этот фильм уже куплены')
#Вывод статистики занято/всего мест
def ProstoStatistica(matrix):
    with open(matrix) as f:
        line = f.readline().split()
        matrix = []
        m = int(line[0])
        n = int(line[1])
        for line in f:
            matrix.append([int(x) for x in line.split()])
    printMatrix(matrix)
    k = n * m
    print('Занято мест:', statistic(matrix), 'Всего мест:', k)
    print()
#Возврат билета
def Sold_bilet(file):
    with open(file) as f:
        line = f.readline().split()
        matrix = []
        m = int(line[0])
        n = int(line[1])
        for line in f:
            matrix.append([int(x) for x in line.split()])
    printMatrix(matrix)
    k = n * m
    print('Занято мест:', statistic(matrix), 'Всего мест:', k)
    print('Введите ряд и место с вашего билета')
    besk = 1
    while besk > 0:
        x = int(input('Введите ряд: '))
        y = int(input('Введите место: '))
        if matrix[x - 1][y - 1] == 1:
            matrix[x - 1][y - 1] = 0
            Zapolnenie(file, matrix)
            break
        else:
            print('Вы обманываете, пожайлуйста введите действительный билет')
    print('Билет сдан')
    print('Теперь, занято мест:', statistic(matrix), 'Всего мест:', k)
#Изменение текстового файла
def Zapolnenie(file, massiv):
    f = open(file, 'w')
    f.write(str(len(massiv[0])) + ' ' + str(len(massiv)))
    f.write('\n')
    for i in range(len(massiv)):
        for j in range(len(massiv[i])):
            f.write(str(massiv[i][j]) + ' ')
        f.write('\n')
    f.close()
print('Приветствуем в кинотеатре vshitis, у нас единая цена на все фильмы, 300 рублей')
besk_2 = 1
while besk_2 > 0:
    print(
        'Вот список фильмов, которые идут на данный момент (введите номер фильма или команду)')
    print('1 - Стрельцов')
    print('2 - Довод')
    print('3 - После. Глава 2')
    print('4 - Мулан')
    print('5 - Гренландия')
    print('6 - Вывод статистики')
    print('7 - Сдать билет')
    print('Выход')

    n = input() #Ввод желаемой услуги (выше)

    if n == '7':
        print('Билет на какой фильм вы хотите сдать?(введите номер)')
        a = input()
        if a == '1':
            Sold_bilet('Film_1.txt')
        if a == '2':
            Sold_bilet('Film_2.txt')
        if a == '3':
            Sold_bilet('Film_3.txt')
        if a == '4':
            Sold_bilet('Film_4.txt')
        if a == '5':
            Sold_bilet('Film_5.txt')

    elif n == '6':
        # Первый фильм
        print('Стрельцов:')
        ProstoStatistica('Film_1.txt')
        # Второй фильм
        print('Довод:')
        ProstoStatistica('Film_2.txt')
        # Третий фильм
        print('После. Глава 2:')
        ProstoStatistica('Film_3.txt')
        # Четвертый фильм
        print('Мулан:')
        ProstoStatistica('Film_4.txt')
        # Пятый фильм
        print('Гренландия: ')
        ProstoStatistica('Film_5.txt')
    elif n == '1':
        Poisk_mesta('Film_1.txt')

    elif n == '2':
        Poisk_mesta('Film_2.txt')

    elif n == '3':
        Poisk_mesta('Film_3.txt')

    elif n == '4':
        Poisk_mesta('Film_4.txt')

    elif n == '5':
        Poisk_mesta('Film_5.txt')
    elif n == 'Выход':
            exit(0)
    else:
        print('Неправильно ввели название :(')
    print('Хотите продолжить работу да/нет?')
    n = input()
    if n == 'нет':
        exit(0)
